# What's this
A super simple app using flask that authenticates your basic auth token.  
It's one way to do it ;)  
A better way would be to let your http server do the basic auth, if necessary.  
Or use a more advanced authentication method (probably using JWT).  

Basically it just reads a file with all your tokens, and if the Basic auth header matches one of the allowed tokens, then it's accepted. Otherwise, it returns an auth error.
The only current action (route) it has is for a csv file.

# Usage
Just put this in a random dir, and you'll be able to run it with uwsgi.  
## the simple way
Very simply put:
```
cd $THIS_DIR
uwsgi --ini wsgi.ini

# or just
cd $THIS_DIR
python wsgi.py
```
## the normal way
Normally though, I guess you'd use a systemd service script, and do
```
service your_uwsgi_service_name start
```
instead though.  
The service script would probably look something like:
```
[Unit]
Description=uWSGI instance for my app doing XXX

[Service]
WorkingDirectory=/usr/local/{{pyvenv_local_app_name}}/repo
ExecStart=/usr/bin/bash -c 'cd /usr/local/{{pyvenv_local_app_name}}; source bin/activate; cd repo; uwsgi --ini wsgi.ini'
ExecReload=/usr/bin/bash -c ' uwsgi --reload /usr/local/{{pyvenv_local_app_name}}/repo/{{pyvenv_local_app_name}}.pid' 
ExecStop=/usr/bin/bash -c 'uwsgi --stop /usr/local/{{pyvenv_local_app_name}}/repo/{{pyvenv_local_app_name}}.pid' 

[Install]
WantedBy=multi-user.target
```
(And deploy with ansible would be nice)

# Pip requirements
1. wsgi
2. pyyaml
3. flask

# See it in action

```
# the default username/password is my_test_user:my_test_password
# so you can just put the header directly
curl -H Authorization\:\ Basic\ b3NzX3VzZXI6ZWlnMnpvaHB1b2Zvb3NhaWZ1R2g=  http://localhost/my_csv_sample

# or tell curl the credentials with the "-u" switch
curl -umy_test_user:my_test_password  http://localhost/my_csv_sample

```
You probably will and should be using https, running this with a web server (nginx) using the uwsgi app. If the data is not encrypted, your credentials could be easily sniffed.
