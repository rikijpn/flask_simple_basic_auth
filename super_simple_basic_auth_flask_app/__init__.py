from flask import Flask, render_template, url_for, request, send_from_directory
from subprocess import run
from flask import make_response
import yaml

app = Flask(__name__, template_folder='views')


def get_my_random_csv_file():
    "gets some csv file's contents"
    content = ""
    with open('super_simple_basic_auth_flask_app/static/some_csv_file.csv', 'r') as f:
        content = f.read()
    return content


def confirm_auth():
    "validates auth token"
    with open("super_simple_basic_auth_flask_app/allowed_tokens.yml") as file:
        allowed_tokens = yaml.load(file, Loader=yaml.FullLoader)
    auth_header = request.headers.get('Authorization')
    allowed = False
    try:
        header_token_part = auth_header.split("Basic ")[1]
    except AttributeError:
        header_token_part = 'invalid'
    if header_token_part in allowed_tokens:
        allowed = True
    return allowed


def get_authorization():
    "super basic auth function, probably something a lot better out there"
    token_ok = confirm_auth()
    if not token_ok:
        response_text = {
            'status': 'fail',
            'message': 'Provide a valid auth token.'
        }
        return make_response(response_text), 401
    else:
        return True


@app.route('/my_csv_sample')
def get_my_csv_sample():
    authorization = get_authorization()
    if authorization != True:
        return authorization
    csv_output = get_my_random_csv_file()
    output = make_response(csv_output)
    output.headers["Content-type"] = "text/csv"
    return output


if __name__ == "__main__":
    app.run()
